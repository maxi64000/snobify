class Search {
  constructor(node, searchMusic) {
    this.node = node;
    this.searchMusic = searchMusic;
  }

  handleKeyPress = () => {
    this.searchMusic(this.input.value);
  };

  renderInput() {
    this.input = document.createElement('input');
    this.input.classList.add('snobify-search-input');

    this.input.setAttribute('placeholder', 'Search...');

    this.input.addEventListener('keyup', this.handleKeyPress);

    this.search.appendChild(this.input);
  }

  render() {
    this.search = document.getElementById('SnobifySearch');

    if (!this.search) {
      this.search = document.createElement('div');
      this.search.setAttribute('id', 'SnobifySearch');
      this.search.classList.add('snobify-search');
      this.node.appendChild(this.search);

      this.renderInput();
    }
  }
}

export default Search;
