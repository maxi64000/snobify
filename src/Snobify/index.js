import Header from '../Header';
import Search from '../Search';
import SearchResult from '../SearchResult';
import Playlist from '../Playlist';
import Player from '../Player';

class Snobify {
  constructor(node) {
    this.node = node;
    this.state = {
      searchMusicList: [],
      playlist: [],
      currentMusicUrl: null,
    };
  }

  playlistMusicIsInSearchResult(index) {
    this.state.searchMusicList.forEach(searchMusic => {
      if (this.state.playlist[index].name === searchMusic.name) {
        return true;
      }
    });

    return false;
  }

  searchMusic = searchText => {
    this.state.searchMusicList = [];

    fetch(`https://api.spotify.com/v1/search?q=${searchText}&type=track`, {
      method: 'GET',
      headers: new Headers({
        Authorization: `Bearer BQDufryLDkb2O9fweXIhE1VrJoANu-BMScsPtyOkPdwWfH_hqHZyusKdZk24iBlS2Z8D4kCNO8Q9-HVO429q-BJPQvkdsFAHPvXyApQql6U-Um3W2xadKVP4P2gN-oBhXPRw3yR_2CiKX9pS`,
        'Content-Type': 'application/json',
      }),
      mode: 'cors',
      cache: 'default',
    })
      .then(response => response.json())
      .then(response => {
        if (!response.error) {
          this.state.searchMusicList = response.tracks.items.map(({ name, id, external_urls }) => ({
            name,
            id,
            isAdded: false,
            src: external_urls.spotify,
          }));
        }

        this.state.searchMusicList.forEach((searchMusic, index) => {
          if (this.state.playlist.map(({ id }) => id).includes(searchMusic.id)) {
            this.state.searchMusicList[index].isAdded = true;
          }
        });

        this.render();
      });
  };

  addMusic = index => {
    this.state.playlist.push(this.state.searchMusicList[index]);
    this.state.searchMusicList[index].isAdded = true;
    this.render();
  };

  deleteMusic = index => {
    if (!this.playlistMusicIsInSearchResult(index)) {
      this.state.searchMusicList.forEach((searchMusic, searchIndex) => {
        if (searchMusic.id === this.state.playlist[index].id) {
          this.state.searchMusicList[searchIndex].isAdded = false;
        }
      });
    }
    this.state.playlist.splice(index, 1);
    this.render();
  };

  deletePlaylist = () => {
    this.state.playlist = [];
    this.state.searchMusicList.forEach((music, index) => {
      this.state.searchMusicList[index].isAdded = false;
    });
    this.render();
  };

  updateMusicUrl = url => {
    this.state.currentMusicUrl =
      'https://open.spotify.com/embed/' + url.split('https://open.spotify.com/')[1];
    this.render();
  };

  render() {
    let snobify = document.getElementById('Snobify');

    if (!snobify) {
      snobify = document.createElement('div');
      snobify.setAttribute('id', 'Snobify');
      snobify.classList.add('snobify');
      this.node.appendChild(snobify);
    }

    new Header(snobify, this.state.playlist.length).render();
    new Player(snobify, this.state.currentMusicUrl).render();
    new Search(snobify, this.searchMusic).render();
    new SearchResult(snobify, this.state.searchMusicList, this.addMusic).render();
    new Playlist(
      snobify,
      this.state.playlist,
      this.deleteMusic,
      this.deletePlaylist,
      this.updateMusicUrl,
    ).render();
  }
}

export default Snobify;
