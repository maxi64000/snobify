class Player {
  constructor(node, src) {
    this.node = node;
    this.src = src;
  }

  render() {
    let player = document.getElementById('SnobifyPlayer');

    if (!player) {
      if (this.src) {
        player = document.createElement('iframe');
        player.setAttribute('id', 'SnobifyPlayer');
        player.src = this.src;
        player.width = 300;
        player.height = 300;
        player.frameBorder = 0;
        player.allowtransparency = true;
        player.allow = 'encrypted-media';
        this.node.appendChild(player);
      }
    } else {
      if (player.src !== this.src) {
        player.src = this.src;
      }
    }
  }
}

export default Player;
