import Snobify from '../Snobify';

class App {
  constructor(node) {
    this.node = node;
  }

  render() {
    new Snobify(this.node).render();
  }
}

export default App;
