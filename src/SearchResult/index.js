class SearchResult {
  constructor(node, searchMusicList, addMusic) {
    this.node = node;
    this.searchMusicList = searchMusicList;
    this.addMusic = addMusic;
  }

  removeMusicList() {
    const musicList = document.getElementsByClassName('snobify-search-music');
    [...musicList].forEach(music => music.remove());
  }

  renderMusicList() {
    const id = 'snobifySearchMusicList';

    this.musicList = document.getElementById(id);

    if (!this.musicList) {
      this.musicList = document.createElement('ul');
      this.musicList.setAttribute('id', id);
      this.musicList.classList.add('snobify-search-music-list');
      this.searchResult.appendChild(this.musicList);
    }

    this.removeMusicList();

    this.searchMusicList.forEach(this.renderMusic);
  }

  renderMusic = (music, index) => {
    this.musicNode = document.createElement('li');
    this.musicNode.classList.add('snobify-search-music');

    const musicName = document.createTextNode(music.name);
    this.musicNode.appendChild(musicName);

    this.musicList.appendChild(this.musicNode);

    this.renderButton(index);
  };

  renderButton(index) {
    const button = document.createElement('button');
    button.classList.add('snobify-search-music-button');

    let text = 'Add';

    if (this.searchMusicList[index].isAdded) {
      text = 'Added';
      button.disabled = true;
    } else {
      button.addEventListener('click', () => this.handleButtonClick(index));
    }

    const buttonText = document.createTextNode(text);
    button.appendChild(buttonText);

    this.musicNode.appendChild(button);
  }

  handleButtonClick(index) {
    this.addMusic(index);
  }

  render() {
    this.searchResult = document.getElementById('SnobifySearchResult');

    if (!this.searchResult) {
      this.searchResult = document.createElement('div');
      this.searchResult.setAttribute('id', 'SnobifySearchResult');
      this.searchResult.classList.add('snobify-search-result');
      this.node.appendChild(this.searchResult);
    }

    this.renderMusicList();
  }
}

export default SearchResult;
