class Playlist {
  constructor(node, playlistMusicList, deleteMusic, deletePlaylist, updateMusicUrl) {
    this.node = node;
    this.playlistMusicList = playlistMusicList;
    this.deleteMusic = deleteMusic;
    this.deletePlaylist = deletePlaylist;
    this.updateMusicUrl = updateMusicUrl;
  }

  renderTitle() {
    const title = document.createElement('h2');
    title.classList.add('snobify-playlist-title');

    const titleContent = document.createTextNode('Playlist');
    title.appendChild(titleContent);

    this.playlist.appendChild(title);
  }

  renderMusicList() {
    this.musicList = document.getElementById('snobifyPlaylistMusicList');

    if (!this.musicList) {
      this.musicList = document.createElement('ul');
      this.musicList.setAttribute('id', 'snobifyPlaylistMusicList');
      this.musicList.classList.add('snobify-playlist-music-list');
      this.playlist.appendChild(this.musicList);
    }

    const musicsNodes = document.getElementsByClassName('snobify-playlist-music');
    [...musicsNodes].forEach(musicNode => musicNode.remove());

    this.playlistMusicList.forEach(this.renderMusic);
  }

  renderMusic = (music, index) => {
    this.musicNode = document.createElement('li');
    this.musicNode.classList.add('snobify-playlist-music');

    const musicName = document.createTextNode(music.name);
    this.musicNode.appendChild(musicName);

    this.musicList.appendChild(this.musicNode);

    this.renderPlayerButton(index);
    this.renderButton(index);
  };

  renderButton(index) {
    const button = document.createElement('button');
    button.classList.add('snobify-search-music-button');

    const buttonText = document.createTextNode('Delete');
    button.appendChild(buttonText);

    button.addEventListener('click', () => this.deleteMusic(index));

    this.musicNode.appendChild(button);
  }

  renderPlayerButton(index) {
    const button = document.createElement('button');
    button.classList.add('snobify-search-music-button');

    const buttonText = document.createTextNode('Play');
    button.appendChild(buttonText);

    button.addEventListener('click', () => this.updateMusicUrl(this.playlistMusicList[index].src));

    this.musicNode.appendChild(button);
  }

  renderDeleteButton() {
    const button = document.createElement('button');
    button.classList.add('snobify-search-music-button');

    const buttonText = document.createTextNode('Delete playlist');
    button.appendChild(buttonText);

    button.addEventListener('click', () => this.deletePlaylist());

    this.playlist.appendChild(button);
  }

  render() {
    this.playlist = document.getElementById('SnobifyPlaylist');

    if (!this.playlist) {
      this.playlist = document.createElement('div');
      this.playlist.setAttribute('id', 'SnobifyPlaylist');
      this.playlist.classList.add('snobify-playlist');
      this.node.appendChild(this.playlist);

      this.renderTitle();
      this.renderDeleteButton();
    }

    this.renderMusicList();
  }
}

export default Playlist;
