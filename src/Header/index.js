class Header {
  constructor(node, playlistNumber) {
    this.node = node;
    this.playlistNumber = playlistNumber;
  }

  renderTitle() {
    const title = document.createElement('h1');
    title.classList.add('snobify-title');

    const titleContent = document.createTextNode('Snobify');
    title.appendChild(titleContent);

    this.header.appendChild(title);
  }

  renderNumber() {
    const id = 'SnobifyNumber';

    let number = document.getElementById(id);

    if (!number) {
      number = document.createElement('span');
      number.setAttribute('id', id);
      number.classList.add('snobify-number');

      const numberContent = document.createTextNode(this.playlistNumber);
      number.appendChild(numberContent);

      this.header.appendChild(number);
    } else {
      if (number.textContent !== this.playlistNumber) {
        number.textContent = this.playlistNumber;
      }
    }
  }

  render() {
    this.header = document.getElementById('SnobifyHeader');

    if (!this.header) {
      this.header = document.createElement('div');
      this.header.setAttribute('id', 'SnobifyHeader');
      this.header.classList.add('snobify-header');
      this.node.appendChild(this.header);

      this.renderTitle();
    }

    this.renderNumber();
  }
}

export default Header;
