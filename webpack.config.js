const HtmlWebpackPlugin = require('html-webpack-plugin');

const DIR_SRC = __dirname + '/src/';
const DIR_PUBLIC = __dirname + '/dist/';

module.exports = {
  devServer: {
    port: 3000,
  },
  entry: DIR_SRC + 'index.js',
  output: {
    path: DIR_PUBLIC,
    filename: 'app.bundle.js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: DIR_SRC + 'index.html',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: ['eslint-loader', 'babel-loader'],
      },
      {
        test: /\.css$/,
        exclude: /(node_modules)/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
};
